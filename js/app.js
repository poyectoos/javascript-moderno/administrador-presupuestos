const formulario = document.querySelector('#agregar-gasto');
const gastoListado = document.querySelector('#gastos ul');


/*
==================================================
          Presupuesto
==================================================
*/
class Presupuesto {
  constructor(presupuesto) {
    this.presupuesto = Number(presupuesto);
    this.restante = this.presupuesto;
    this.gastos = [];
  }
  agregarGasto(gasto) {
    const {id, nombre, cantidad} = gasto;
    this.gastos = [...this.gastos, gasto];
    this.calcularRestante();
  }
  calcularRestante() {
    const gastado = this.gastos.reduce( (total, gasto) => total + gasto.cantidad, 0);
    this.restante = this.presupuesto - gastado;
  }
  eliminarGasto(id) {
    this.gastos = this.gastos.filter(gasto => gasto.id !== id);
    this.calcularRestante();
  }
}
/*
==================================================
          Interface
==================================================
*/

class UI {
  insertarPresupuesto(objeto) {
    const { presupuesto, restante } = objeto;
    document.querySelector('#total').textContent = presupuesto;
    document.querySelector('#restante').textContent = restante;
  }
  alerta(mensaje, tipo) {
    const alerta = document.createElement('div');
    alerta.textContent = mensaje;
    alerta.classList.add('text-center', 'alert');
    if (tipo === 'error') {
      alerta.classList.add('alert-danger');
    } else if (tipo === 'success') {
      alerta.classList.add('alert-success');
    }
    document.querySelector('.primario').insertBefore(alerta, formulario);

    setTimeout(() => {
      alerta.remove();
    }, 3000);
  }
  listarGastos(gastos) {
    this.limpiarGastos();
    gastos.forEach(gasto => {
      const { id, nombre, cantidad } = gasto;
      const li = document.createElement('li');
      li.classList.add('list-group-item', 'd-flex', 'justify-content-between', 'align-items-center');
      li.dataset.id = id;

      li.innerHTML = `
        ${nombre} <span class="badge badge-primary badge-pill">$ ${cantidad}</span>
      `;

      const borrar = document.createElement('button');
      borrar.classList.add('btn', 'btn-danger', 'borrar-gasto');
      borrar.textContent = 'Borrar';
      borrar.onclick = () => {
        eliminarGasto(id);
      }

      li.appendChild(borrar);

      gastoListado.appendChild(li);
    });
  }
  limpiarGastos(){
    while (gastoListado.firstChild) {
      gastoListado.removeChild(gastoListado.firstChild);
    }
  }
  actualizarRestante(cantidad) {
    const restante = document.querySelector('#restante');
    restante.textContent = cantidad;
  }
  comprobarRestante(objeto) {
    const {presupuesto, restante} = objeto;
    const div = document.querySelector('.restante');

    div.classList.remove('alert-success', 'alert-danger', 'alert-warning');
    div.classList.remove('alert-success');
    if ((presupuesto*0.25) > restante) {
      div.classList.add('alert-danger');
    } else if ((presupuesto*0.5) > restante) {
      div.classList.add('alert-warning');
    } else {
      div.classList.add('alert-success');
    }
    const boton = formulario.querySelector('button');
    if (restante <= 0) {
      ui.alerta('Presupuesto agotado.', 'error');
      boton.disabled = true;
    } else {
      boton.disabled = false;
    }
  }
}

/*
==================================================
          Aplicacion
==================================================
*/

const ui = new UI();
let presupuesto;

document.addEventListener('DOMContentLoaded', main);

function eventListeners() {
  formulario.addEventListener('submit', agregarGasto);
}

function main() {
  pedirPresupuesto();
  eventListeners();
}

function pedirPresupuesto() {
  const cantidad = prompt('¿Cuál es tu presupuesto?');
  //const cantidad = '10000';

  if (cantidad === null) {
    // Si cancela
    window.location.reload();
  } else if (cantidad.trim() === '') {
    // No escribio nada
    window.location.reload();
  } else if (isNaN(Number(cantidad)) || Number(cantidad) <= 0) {
    // Metio letras
    window.location.reload();
  }
  
  presupuesto = new Presupuesto(cantidad);
  ui.insertarPresupuesto(presupuesto);
  /*
  // Lineas para test
  presupuesto.agregarGasto({
    id: 1,
    nombre: 'Comidita de Halley',
    cantidad: 150
  });
  presupuesto.agregarGasto({
    id: 2,
    nombre: 'Comidita de Darwin',
    cantidad: 200
  });

  ui.listarGastos(presupuesto.gastos);
  ui.actualizarRestante(presupuesto.restante);
  */
}
function agregarGasto(e) {
  e.preventDefault();
  const nombre = document.querySelector('#gasto').value.trim();
  const cantidad = document.querySelector('#cantidad').value.trim();

  if (nombre === '' || cantidad === '') {
    ui.alerta('Todos los campos son obligatorios.', 'error');
    return;
  } else if (isNaN(cantidad) || Number(cantidad) <= 0) {
    ui.alerta('No es una cantidad valida.', 'error');
    return;
  }

  // Construimos gasto
  const gasto = {
    id: Date.now(),
    nombre,
    cantidad: Number(cantidad) 
  };

  presupuesto.agregarGasto(gasto);

  ui.alerta('Gasto agregado correctamente.', 'success');
  ui.listarGastos(presupuesto.gastos);
  ui.actualizarRestante(presupuesto.restante);
  ui.comprobarRestante(presupuesto);

  formulario.reset();
}

function eliminarGasto(id) {
  presupuesto.eliminarGasto(id);
  ui.listarGastos(presupuesto.gastos);
  ui.actualizarRestante(presupuesto.restante);
  ui.comprobarRestante(presupuesto);
}